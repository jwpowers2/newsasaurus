var redis = require('redis');


function RedisConn(){

    

	this.createClient = function(){

		var client = redis.createClient()
		client.on('connect', function() {
		    console.log('Redis client connected');
		});

		client.on('error', function (err) {
		    console.log('Something went wrong ' + err);
		});
                return client;
        }
}

module.exports = RedisConn;
