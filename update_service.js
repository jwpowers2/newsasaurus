var getData = require('./getData');
var redis = require('redis');
var MediaList = require('./media_list')
var UpdateMedia = require('./UpdateMedia')

var rclient = redis.createClient();
let updateMedia = new UpdateMedia();
let ml = new MediaList();
rclient.set('ticker','{}');


function setMediaRedis(){

  for(let media in ml.mediaList){
  
    let m = ml.mediaList[media].query.toLowerCase();
    rclient.set(m,'{}');

  }
  
  for(let cat in ml.categoryList){
  
    let m = ml.categoryList[cat].query.toLowerCase();
    rclient.set(m,'{}');

  }
  
  console.log('clean media and cat objects set');
}

setMediaRedis();
rclient.quit();

updateMedia.updateMedia();
updateMedia.updateCategory();

setInterval(function(){
  
    updateMedia.updateMedia();
    console.log('updated media');
    setTimeout(()=>{
      updateMedia.updateCategory();
      console.log('updated category');
    },20000);
},200000);


