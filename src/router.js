import Vue from 'vue'
import Router from 'vue-router'
import home from './views/home.vue'
import homeMobile from './views/homeMobile.vue'
import about from './views/about.vue'
import nsheader from './components/nsheader.vue'
import adheader from './components/adheader.vue'
import nsabout from './components/nsabout.vue'
import newsticker from './components/newsticker.vue'
import trending from './components/trending.vue'
import querywindow from './components/querywindow.vue'
import headlinewindow from './components/headlinewindow.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  //base: process.env.BASE_URL,
  routes: [
    {
      path: '/about',
      name: 'about',
      component: about
    },
    {
      path: '/nsabout',
      name: 'nsabout',
      component: nsabout
    },
    {
      path: '/nsheader',
      name: 'nsheader',
      component: nsheader
    },
    {
      path: '/adheader',
      name: 'adheader',
      component: adheader
    },
    {
      path: '/newsticker',
      name: 'newsticker',
      component: newsticker
    },
    {
      path:'/querywindow',
      name: 'querywindow',
      component: querywindow
    },
    {
      path:'/headlinewindow',
      name: 'headlinewindow',
      component: headlinewindow
    },
    {
      path: '/',
      component: home,
      children: [
        {
        path:'',
        component:trending
        }
      ],
    },
    {
      path: '/homeMobile',
      component: homeMobile,
      children: [
        {
        path:'',
        component:trending
        }
      ],
    }
  ]
})

