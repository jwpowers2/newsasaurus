// controller for Data
var RedisConn = require('../../redisClient.js')
var MediaList = require('../../media_list.js')
let rc = new RedisConn();
var redisClient = rc.createClient();
let mediaList = new MediaList();

class DataController{

  test(req,res){

    console.log("data controller fired");
    // test this endpoint
    redisClient.get('test', function(error,result){    
      res.json({test:result});
    });
  }
  media(req,res){

    //console.log("media request received");
    
    let dataResponse = {};
    let mediaRequested = mediaList.mediaMap[req.params.media];
    // get ticker first and then put headlines get in a try block

    redisClient.get(mediaRequested, function(error,result){    
      //console.log(result);
      dataResponse['media'] = result;
      redisClient.get('ticker', function(error,ticker){

        let t = JSON.parse(ticker);
        dataResponse['ticker'] = t[mediaRequested]
        res.json(dataResponse);
      });
    });
  }

}

module.exports = new DataController();
