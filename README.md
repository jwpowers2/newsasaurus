# Newsasaurus

## Vue-cli-3

## newsapi.org

### installation and setup

    // make sure you have node.js and npm installed

    // installed node.js v8.x on ubuntu 16 xenial

    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

    sudo apt-get install -y nodejs


    node -v    // test version is 8 +

    npm install -g @vue/cli   // install vue-cli-3 globally using npm

    vue --version   // test the version, should be 3 +




## need to run update_service.js with pm2 and run server.js with pm2
#### update_service updates the news data, making news tickers in redis


