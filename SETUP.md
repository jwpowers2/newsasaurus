# setup

## setting up iptables

## install persistent program for iptables so firewall loads on boot

    apt install iptables-persistent

### allow traffic on loopback
    sudo iptables -A INPUT -i lo -j ACCEPT
    sudo iptables -A OUTPUT -o lo -j ACCEPT

### allow established connections
    sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    sudo iptables -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT

### drop invalid connections
    sudo iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

### allow ssh port 22 traffic 
    sudo iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

### allow http and https traffic
    sudo iptables -A INPUT -p tcp -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

### allow redis conn on loopback but this is probably redundant
    sudo iptables -A INPUT -i lo -p tcp --dport 6379 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

### if iptables are flushed default to ACCEPT policy so no lockout of user
    iptables -P INPUT ACCEPT

### catchall to drop bad traffic
    iptables -A INPUT -j DROP

### save config and load
    netfilter-persistent save
